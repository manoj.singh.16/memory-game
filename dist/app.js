const cardWrapper = document.querySelector('#card-wrapper');
const resetButton = document.querySelector('#reset');
const movesCounter = document.querySelector('#moves-counter');
const timeCounter = document.querySelector('#time-counter');
const startBtn = document.querySelector('#start');
const overlay = document.querySelector('#overlay');
const highScoreEle = document.querySelector('#high-score');

let openedCards = [];
let completedSet = 0;
let moves = 0;

let shuffledCards = [];
let intervalId;
let seconds = 0;
let minutes = 0;

const cardFaces = [
  'fas fa-car-side',
  'fas fa-house-user',
  'fab fa-apple',
  'fas fa-leaf',
  'fab fa-linux',
  'fab fa-pagelines',
  'fab fa-js',
  'fab fa-chrome',
  'fas fa-car-side',
  'fas fa-house-user',
  'fab fa-apple',
  'fas fa-leaf',
  'fab fa-linux',
  'fab fa-pagelines',
  'fab fa-js',
  'fab fa-chrome',
];

const createCard = (icon) => {
  const li = document.createElement('li');
  li.classList.add('card');
  li.setAttribute('data-type', icon);
  const i = document.createElement('i');
  i.className += icon;
  li.appendChild(i);
  return li;
};

const shuffleFaces = (cardFaces) => {
  cardFaces.forEach((face, i) => {
    const randomNumber = Math.floor(Math.random() * cardFaces.length);
    const temp = cardFaces[i];
    cardFaces[i] = cardFaces[randomNumber];
    cardFaces[randomNumber] = temp;
  });
  return cardFaces;
};

const setActive = (card) => {
  card.classList.add('active-card', 'disable');
  openedCards.push(card);
  updateMoves(1);
};

const setHighScore = () => {
  const highScore = localStorage.getItem('highScore');
  if (!highScore || moves < highScore) {
    localStorage.setItem('highScore', moves);
  }
};

const checkWin = () => {
  if (completedSet === 8) {
    clearInterval(intervalId);
    overlay.style.display = 'flex';
    const h2 = document.createElement('h2');
    h2.innerHTML = 'You Win!! <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i>';
    const movesH3 = document.createElement('h3');
    movesH3.innerText = `Total Moves: ${moves / 2}`;
    const timerH3 = document.createElement('h3');
    timerH3.innerText = `Time Taken: ${minutes}mins ${seconds} secs`;
    overlay.innerHTML = '';
    overlay.append(h2);
    overlay.append(movesH3);
    overlay.append(timerH3);
    overlay.append(resetButton);

    setHighScore();
  }
};

const startTimer = () => {
  seconds = 0;
  minutes = 0;
  intervalId = setInterval(() => {
    seconds += 1;

    if (seconds === 60) {
      minutes += 1;
      seconds = 0;
    }

    timeCounter.innerText = `${minutes} mins ${seconds} secs
    `;
  }, 1000);
};

const updateMoves = (number) => {
  moves += number;
  if (moves % 2 === 0) {
    movesCounter.innerText = `${moves / 2} Move(s)`;
  }
};

const matchCards = () => {
  if (openedCards.length === 2) {
    if (openedCards[0].getAttribute('data-type') === openedCards[1].getAttribute('data-type')) {
      completedSet += 1;
      openedCards = [];
      checkWin();
    } else {
      cardWrapper.style.pointerEvents = 'none';

      setTimeout(() => {
        openedCards[0].classList.remove('active-card');
        openedCards[1].classList.remove('active-card');
        openedCards[0].classList.remove('disable');
        openedCards[1].classList.remove('disable');
        openedCards = [];
        cardWrapper.style.pointerEvents = 'auto';
      }, 1000 * 0.5);
    }
  }
};

const updateHighScore = () => {
  const highScore = localStorage.getItem('highScore');
  if (highScore) {
    highScoreEle.innerText = `Your high score is ${highScore}`;
  }
};

const reset = (event) => {
  openedCards = [];
  completedSet = 0;
  overlay.style.display = 'none';
  cardWrapper.innerHTML = '';
  clearInterval(intervalId);
  console.log(intervalId);
  timeCounter.innerText = '0 mins 0 secs';
  moves = 0;

  updateMoves(0);

  startGame();
};

resetButton.addEventListener('click', reset);

const startGame = () => {
  const shuffledFaces = shuffleFaces(cardFaces);

  updateHighScore();
  startTimer();

  shuffledCards = shuffledFaces.map((face) => createCard(face));

  shuffledCards.forEach((card) => {
    cardWrapper.appendChild(card);
    card.addEventListener('click', (event) => {
      setActive(card);
      matchCards();
    });
  });
};

updateHighScore();
startBtn.addEventListener('click', () => {
  overlay.style.display = 'none';
  startGame();
});
